defmodule PriorityQueue do
  @moduledoc """
  Basic implementation of a Priority queue.
  """

  defstruct stack: [], type: :lifo

  @type queue_type() :: :fifo | :lifo
  @type t() :: %__MODULE__{
          stack: list(),
          type: queue_type()
        }

  @doc """
  Create a new queue.

  ## Examples

      fifo_queue = PriorityQueue.new(:fifo)
      lifo_queue = PriorityQueue.new(:lifo)
  """
  @spec new(queue_type()) :: t()
  def new(type), do: %__MODULE__{type: type}

  @doc """
  Check if the queue is empty
  """
  @spec empty?(t()) :: boolean()
  def empty?(%__MODULE__{stack: []}), do: true
  def empty?(%__MODULE__{}), do: false

  @doc """
  Return the first value from a queue.
  Returns `nil` if the queue is empty.

  ## Examples

      head = PriorityQueue.peek(queue)
  """
  @spec peek(t()) :: term() | nil
  def peek(%__MODULE__{stack: []}), do: nil
  def peek(%__MODULE__{stack: [{_, [head | _]} | _]}), do: head

  @doc """
  Return a tuple with the first value from a queue
  and a new queue with that value removed.
  Returns `nil` if the queue is empty.

  ## Examples

      {value, new_queue} = PriorityQueue.pop(queue)
  """
  @spec pop(t()) :: {term() | nil, t()}
  def pop(%__MODULE__{stack: []} = queue), do: {nil, queue}

  def pop(%__MODULE__{stack: [{_, [head | []]} | tail]} = queue) do
    {head, %{queue | stack: tail}}
  end

  def pop(%__MODULE__{stack: [{priority, [head | next]} | tail]} = queue) do
    next_stack = [{priority, next} | tail]
    {head, %{queue | stack: next_stack}}
  end

  @doc """
  Insert a new value in the specified priority in the queue.
  Returns the new queue.

  ## Examples

      new_queue = PriorityQueue.push(queue, 12, 2)
  """
  @spec push(t(), term(), integer()) :: t()
  def push(%__MODULE__{stack: stack, type: type} = queue, item, priority) do
    {fun, index, value} = build_next(stack, item, priority, type)
    %{queue | stack: fun.(stack, index, value)}
  end

  @doc """
  Turn the queue into a list.
  """
  @spec to_list(t()) :: list()
  def to_list(%__MODULE__{stack: stack}) do
    Enum.flat_map(stack, fn {_, list} -> list end)
  end

  # Private functions

  defp build_next(stack, item, priority, type, index \\ 0)

  defp build_next([], item, priority, _, _) do
    {&List.insert_at/3, -1, {priority, [item]}}
  end

  defp build_next([{priority, stack} | _], item, priority, type, index) do
    next_stack =
      case type do
        :fifo -> stack ++ [item]
        :lifo -> [item | stack]
      end

    {&List.replace_at/3, index, {priority, next_stack}}
  end

  defp build_next([{next_priority, _} | _], item, priority, _, index)
       when priority < next_priority do
    {&List.insert_at/3, index, {priority, [item]}}
  end

  defp build_next([_ | tail], item, priority, type, index) do
    build_next(tail, item, priority, type, index + 1)
  end
end
