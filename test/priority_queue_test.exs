defmodule PriorityQueueTest do
  use ExUnit.Case
  doctest PriorityQueue

  setup_all queue do
    fifo = PriorityQueue.new(:fifo)
    lifo = PriorityQueue.new(:lifo)

    %{fifo: fifo, lifo: lifo}
  end

  # General tests
  test "is initialised as empty" do
    queue = PriorityQueue.new(:lifo)
    assert PriorityQueue.peek(queue) == nil
  end

  test "pops nil when empty", %{lifo: queue} do
    {result, _} = PriorityQueue.pop(queue)
    assert result == nil
  end

  test "empty returns the right value", %{lifo: queue} do
    assert PriorityQueue.empty?(queue) == true

    new_queue = PriorityQueue.push(queue, 1, 1)
    assert PriorityQueue.empty?(new_queue) == false

    new_queue = PriorityQueue.push(new_queue, 2, 1)
    assert PriorityQueue.empty?(new_queue) == false
  end

  test "to_list returns a list", %{lifo: queue} do
    queue =
      queue
      |> PriorityQueue.push(1, 3)
      |> PriorityQueue.push(2, 3)
      |> PriorityQueue.push(3, 2)
      |> PriorityQueue.push(4, 1)

    assert PriorityQueue.to_list(queue) == [4, 3, 2, 1]
  end

  # LIFO tests
  test "lifo pops last in first", %{lifo: lifo} do
    {result, new_queue} =
      lifo
      |> PriorityQueue.push("first", 1)
      |> PriorityQueue.push("second", 1)
      |> PriorityQueue.pop()

    assert result == "second"
    assert PriorityQueue.peek(new_queue) == "first"
  end

  test "lifo priority is respected", %{lifo: lifo} do
    queue =
      lifo
      |> PriorityQueue.push("first", 1)
      |> PriorityQueue.push("second", 2)
      |> PriorityQueue.push("third", 1)
      |> PriorityQueue.push("fourth", 200)

    assert PriorityQueue.peek(queue) == "third"

    {result, new_queue} = PriorityQueue.pop(queue)
    assert result == "third"
    {result, new_queue} = PriorityQueue.pop(new_queue)
    assert result == "first"
    {result, new_queue} = PriorityQueue.pop(new_queue)
    assert result == "second"
    {result, _} = PriorityQueue.pop(new_queue)
    assert result == "fourth"
  end

  # FIFO tests
  test "fifo pops first in first", %{fifo: fifo} do
    {result, new_queue} =
      fifo
      |> PriorityQueue.push("first", 1)
      |> PriorityQueue.push("second", 1)
      |> PriorityQueue.pop()

    assert result == "first"
    assert PriorityQueue.peek(new_queue) == "second"
  end

  test "fifo priority is respected", %{fifo: fifo} do
    queue =
      fifo
      |> PriorityQueue.push("first", 1)
      |> PriorityQueue.push("second", 200)
      |> PriorityQueue.push("third", 1)
      |> PriorityQueue.push("fourth", 2)

    assert PriorityQueue.peek(queue) == "first"

    {result, new_queue} = PriorityQueue.pop(queue)
    assert result == "first"
    {result, new_queue} = PriorityQueue.pop(new_queue)
    assert result == "third"
    {result, new_queue} = PriorityQueue.pop(new_queue)
    assert result == "fourth"
    {result, _} = PriorityQueue.pop(new_queue)
    assert result == "second"
  end
end
